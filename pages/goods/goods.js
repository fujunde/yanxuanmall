// pages/goods/goods.js
var WxParse = require('../../lib/wxParse/wxParse.js');
var app = getApp();
var util = require('../../utils/util.js');
var api = require('../../config/api.js');

Page({
    data: {
        id: 0,
        goods: {},
        gallery: [],
        attribute: [],
        issueList: [],
        comment: [],
        brand: {},
        specificationList: [],
        productList: [],
        relatedGoods: [],
        cartGoodsCount: 0,
        userHasCollect: 0,
        number: 1,
        checkedSpecText: '请选择规格数量',
        openAttr: false,
        noCollectImage: "/static/images/icon_collect.png",
        hasCollectImage: "/static/images/icon_collect_checked.png",
        collectBackImage: "/static/images/icon_collect.png",
        posterConfig: {
            width: 750,
            height: 1334,
            backgroundColor: '#fff',
            debug: false,
            pixelRatio: 1,
            blocks: [{
                    width: 690,
                    height: 808,
                    x: 30,
                    y: 183,
                    borderWidth: 2,
                    borderColor: '#f0c2a0',
                    borderRadius: 20,
                },
                {
                    width: 634,
                    height: 74,
                    x: 59,
                    y: 770,
                    backgroundColor: '#fff',
                    opacity: 0.5,
                    zIndex: 100,
                },
            ],
            texts: [{
                    x: 113,
                    y: 61,
                    baseLine: 'middle',
                    text: '伟仔',
                    fontSize: 32,
                    color: '#8d8d8d',
                },
                {
                    x: 30,
                    y: 113,
                    baseLine: 'top',
                    text: '发现一个好物，推荐给你呀',
                    fontSize: 38,
                    color: '#080808',
                },
                {
                    x: 92,
                    y: 810,
                    fontSize: 38,
                    baseLine: 'middle',
                    text: '标题标题标题标题标题标题标题标题标题',
                    width: 570,
                    lineNum: 1,
                    color: '#8d8d8d',
                    zIndex: 200,
                },
                {
                    x: 59,
                    y: 895,
                    baseLine: 'middle',
                    text: [{
                            text: '2人拼',
                            fontSize: 28,
                            color: '#ec1731',
                        },
                        {
                            text: '¥99',
                            fontSize: 36,
                            color: '#ec1731',
                            marginLeft: 30,
                        }
                    ]
                },
                {
                    x: 522,
                    y: 895,
                    baseLine: 'middle',
                    text: '已拼2件',
                    fontSize: 28,
                    color: '#929292',
                },
                {
                    x: 59,
                    y: 945,
                    baseLine: 'middle',
                    text: [{
                            text: '商家发货&售后',
                            fontSize: 28,
                            color: '#929292',
                        },
                        {
                            text: '七天退货',
                            fontSize: 28,
                            color: '#929292',
                            marginLeft: 50,
                        },
                        {
                            text: '运费险',
                            fontSize: 28,
                            color: '#929292',
                            marginLeft: 50,
                        },
                    ]
                },
                {
                    x: 360,
                    y: 1065,
                    baseLine: 'top',
                    text: '长按识别小程序码',
                    fontSize: 38,
                    color: '#080808',
                },
                {
                    x: 360,
                    y: 1123,
                    baseLine: 'top',
                    text: '超值好货一起拼',
                    fontSize: 28,
                    color: '#929292',
                },
            ],
            images: [{
                    width: 62,
                    height: 62,
                    x: 30,
                    y: 30,
                    borderRadius: 62,
                    url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/02bb99132352b5b5dcea.jpg',
                },
                {
                    width: 634,
                    height: 634,
                    x: 59,
                    y: 210,
                    url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/193256f45999757701f2.jpeg',
                },
                {
                    width: 220,
                    height: 220,
                    x: 92,
                    y: 1020,
                    url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/d719fdb289c955627735.jpg',
                },
                {
                    width: 750,
                    height: 90,
                    x: 0,
                    y: 1244,
                    url: 'https://lc-I0j7ktVK.cn-n1.lcfile.com/67b0a8ad316b44841c69.png',
                }
            ]
        }
    },
    onPosterSuccess(e) {
        const {
            detail
        } = e;
        wx.previewImage({
            current: detail,
            urls: [detail]
        })
    },
    onPosterFail(err) {
        console.error(err);
    },


    //获取数据
    getGoodsInfo() {
        util.request(api.GoodsDetail, {
            id: this.data.id
        }).then(res => {
            this.setData({
                goods: res.data.info,
                gallery: res.data.gallery,
                attribute: res.data.attribute,
                issueList: res.data.issue,
                comment: res.data.comment,
                brand: res.data.brand,
                specificationList: res.data.specificationList,
                productList: res.data.productList,
                userHasCollect: res.data.userHasCollect
            });
            if (res.data.userHasCollect == 1) {
                this.setData({
                    'collectBackImage': this.data.hasCollectImage
                });
            } else {
                this.setData({
                    'collectBackImage': this.data.noCollectImage
                });
            }
            WxParse.wxParse('goodsDetail', 'html', res.data.info.goods_desc, this);
            this.getGoodsRelated()
        })
    },
    //获取数据
    getGoodsRelated() {
        util.request(api.GoodsRelated, {
            id: this.data.id
        }).then((res) => {
            this.setData({
                relatedGoods: res.data.goodsList,
            });
        });
    },
    //获取数据
    getCartGoodsCount() {
        util.request(api.CartGoodsCount).then((res) => {
            this.setData({
                cartGoodsCount: res.data.cartTotal.goodsCount
            });
        });
    },
    //选择商品规格
    switchAttrPop() {
        if (this.data.openAttr == false) {
            this.setData({
                openAttr: !this.data.openAttr
            });
        }
    },
    //跳转
    gotoRelatedItem(e) {
        wx.navigateTo({
            url: `/pages/goods/goods?id=${e.currentTarget.dataset.id}`,
        })
    },
    closeAttr() {
        this.setData({
            openAttr: false
        })
    },
    //商品数量 减
    cutNumber() {
        this.setData({
            number: this.data.number > 1 ? this.data.number - 1 : 1
        });
    },
    //商品数量 加
    addNumber() {
        this.setData({
            number: this.data.number + 1
        });
    },
    //添加或取消收藏
    addCannelCollect() {
        util.request(api.CollectAddOrDelete, {
            typeId: 0,
            valueId: this.data.id
        }, "POST").then(res => {
            if (res.data.type == 'add') {
                this.setData({
                    collectBackImage: this.data.hasCollectImage
                });
            } else {
                this.setData({
                    collectBackImage: this.data.noCollectImage
                });
            }
        }).catch(res => {

        })
    },
    openCartPage() {
        wx.switchTab({
            url: '/pages/cart/cart',
        });
    },
    //加入购物车
    addToCart() {
        if (this.data.openAttr == false) {
            this.setData({
                openAttr: !this.data.openAttr
            });
        } else {
            util.request(api.CartAdd, {
                goodsId: this.data.goods.id,
                number: this.data.number,
                productId: this.data.productList[0].id
            }, "POST").then(res => {
                wx.showToast({
                    title: '添加成功'
                });
                this.setData({
                    openAttr: !this.data.openAttr,
                    cartGoodsCount: res.data.cartTotal.goodsCount
                });
            }).catch(err => {
                console(err)
            })
        }
    },
    onLoad(options) {
        this.setData({
            id: options.id
        });
        this.getGoodsInfo();
        this.getCartGoodsCount()
    },
    onShareAppMessage() {
        return {
            title: '自定义转发标题',
        }
    }
})