// pages/catalog/catalog.js
const util = require('../../utils/util.js');
const api = require('../../config/api.js');
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        navList: [],
        categoryList: [],
        currentCategory: {},
        scrollLeft: 0,
        scrollTop: 0,
        goodsCount: 0,
        scrollHeight: 0
    },
    async getCatalog() {
        wx.showToast({
            title: '加载中...',
            icon: 'loading',
        });
        await util.request(api.CatalogList).then((res) => {
            this.setData({
                navList: res.data.categoryList,
                currentCategory: res.data.currentCategory
            })
        });
        await util.request(api.GoodsCount).then((res) => {
            this.setData({
                goodsCount: res.data.goodsCount
            });
        });
        wx.hideToast()
    },
    async getCatalogCurrent(id) {
        await util.request(api.CatalogCurrent, {
            id:id
        }).then((res) => {
            this.setData({
                currentCategory: res.data.currentCategory
            });
        });
    },
    switchCate(e) {
        var id = e.currentTarget.dataset.id
        if (this.data.currentCategory.id != id) {
            this.getCatalogCurrent(id)
        }
    },
    navigatorItem(e) {
        wx.navigateTo({
          url: `/pages/category/category?id=${e.currentTarget.dataset.id}`,
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad() {
        this.getCatalog()
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})