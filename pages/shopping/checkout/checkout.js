// pages/shopping/checkout/checkout.js
const util = require('../../../utils/util.js')
const api = require('../../../config/api.js')
//获取应用实例
const app = getApp()

Page({
    data: {
        checkedGoodsList: [],
        checkedAddress: {},
        checkedCoupon: [],
        couponList: [],
        goodsTotalPrice: 0.00, //商品总价
        freightPrice: 0.00, //快递费
        couponPrice: 0.00, //优惠券的价格
        orderTotalPrice: 0.00, //订单总价
        actualPrice: 0.00, //实际需要支付的总价
        addressId: 0,
        couponId: 0
    },
    //获取数据
    getCheckoutInfo() {
        util.request(api.CartCheckout, {
            addressId: this.data.addressId,
            couponId: this.data.couponId
        }).then(res => {
            this.setData({
                checkedGoodsList: res.data.checkedGoodsList,
                checkedAddress: res.data.checkedAddress,
                actualPrice: res.data.actualPrice,
                checkedCoupon: res.data.checkedCoupon,
                couponList: res.data.couponList,
                couponPrice: res.data.couponPrice,
                freightPrice: res.data.freightPrice,
                goodsTotalPrice: res.data.goodsTotalPrice,
                orderTotalPrice: res.data.orderTotalPrice
            });
            wx.hideLoading();
        })
    },
    //修改地址
    selectAddress() {
        wx.navigateTo({
            url: '/pages/shopping/address/address',
        })
    },
    //添加地址
    addAddress() {
        wx.navigateTo({
            url: '/pages/shopping/addressAdd/addressAdd',
        })
    },
    //去付款
    submitOrder() {
        if (this.data.addressId <= 0) {
            wx.showToast({
                title: '请选择收货地址',
                image: '/static/images/icon_error.png'
            })
        }else{
            
        }
    },
    onShow() {
        wx.showLoading({
            title: '加载中',
        })
        this.getCheckoutInfo();
    }
})