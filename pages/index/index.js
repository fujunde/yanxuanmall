//index.js
const util = require('../../utils/util.js')
const api = require('../../config/api.js')
//获取应用实例
const app = getApp()

Page({
  data: {
    goodsCount: 0,
    newGoods: [],
    hotGoods: [],
    topics: [],
    brands: [],
    floorGoods: [],
    banner: [],
    channel: []
  },
  getIndexData() {
    util.request(api.IndexUrl).then(res => {
      this.setData({
        newGoods: res.data.newGoodsList,
        hotGoods: res.data.hotGoodsList,
        topics: res.data.topicList,
        brand: res.data.brandList,
        floorGoods: res.data.categoryList,
        banner: res.data.banner,
        channel: res.data.channel
      });
    })
  },
  getGoodsCount() {
    util.request(api.GoodsCount).then(res => {
      this.setData({
        goodsCount: res.data.goodsCount
      });
    });
  },
  onLoad() {
    this.getIndexData();
    this.getGoodsCount()
  }
})