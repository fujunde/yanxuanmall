// pages/cart/cart.js
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        cartGoods: [],
        cartTotal: {
            "goodsCount": 0,
            "goodsAmount": 0.00,
            "checkedGoodsCount": 0,
            "checkedGoodsAmount": 0.00
        },
        isEditCart: false,
        checkedAllStatus: null,
    },
    //获取商品数据
    getCartList() {
        util.request(api.CartList).then(res => {
            this.setData({
                cartGoods: res.data.cartList,
                cartTotal: res.data.cartTotal,
            });
            this.setData({
                checkedAllStatus: this.isCheckedAll()
            })
        })
    },
    //商品勾选
    checkedItem(e) {
        var id = e.currentTarget.dataset.itemIndex;
        if (this.data.isEditCart) {
            var tmpCartData = this.data.cartGoods.map((item, index) => {
                if (index == id) {
                    item.checked = !item.checked;
                }
                return item;
            });
            this.setData({
                cartGoods: tmpCartData,
                checkedAllStatus: this.isCheckedAll(),
                'cartTotal.checkedGoodsCount': this.getCheckedGoodsCount()
            });
        } else {
            util.request(api.CartChecked, {
                productIds: this.data.cartGoods[id].product_id,
                isChecked: this.data.cartGoods[id].checked ? 0 : 1
            }, 'POST').then(res => {
                this.setData({
                    cartGoods: res.data.cartList,
                    cartTotal: res.data.cartTotal,
                });
                this.setData({
                    checkedAllStatus: this.isCheckedAll()
                })
            })
        }
    },
    //判断购物车商品全选
    isCheckedAll() {
        return this.data.cartGoods.every(item => item.checked);
    },
    //全选
    checkedAll() {
        if (this.data.isEditCart) {
            var boo = this.isCheckedAll();
            console.log(boo)
            let tmpCartData = this.data.cartGoods.map(item => {
                item.checked = !boo;
                return item;
            });
            this.setData({
                cartGoods: tmpCartData,
                checkedAllStatus: this.isCheckedAll(),
                'cartTotal.checkedGoodsCount': this.getCheckedGoodsCount()
            });
        } else {
            var productIds = this.data.cartGoods.map((item) => {
                return item.product_id;
            });
            util.request(api.CartChecked, {
                productIds: productIds.join(','),
                isChecked: this.isCheckedAll() ? 0 : 1
            }, 'POST').then((res) => {
                this.setData({
                    cartGoods: res.data.cartList,
                    cartTotal: res.data.cartTotal
                });
                this.setData({
                    checkedAllStatus: this.isCheckedAll()
                });
            });
        }
    },
    //编辑
    editCart() {
        if (this.data.isEditCart) {
            this.getCartList();
            this.setData({
                isEditCart: !this.data.isEditCart
            });
        } else {
            let tmpCartList = this.data.cartGoods.map(item => {
                item.checked = false;
                return item;
            });
            this.setData({
                cartGoods: tmpCartList,
                isEditCart: !this.data.isEditCart,
                checkedAllStatus: this.isCheckedAll(),
                'cartTotal.checkedGoodsCount': 0
            });
        }
    },
    //全选的数量
    getCheckedGoodsCount() {
        var val = 0
        this.data.cartGoods.forEach(item => {
            if (item.checked) {
                val += item.number
            }
        })
        return val
    },
    //商品数量 减
    cutNumber(e) {
        var id = e.currentTarget.dataset.itemIndex;
        var item = this.data.cartGoods[id]
        var number = item.number > 1 ? item.number - 1 : 1
        item.number = number
        this.setData({
            cartGoods: this.data.cartGoods
        })
        this.updateCart(item.product_id, item.goods_id, number, item.id)
    },
    //商品数量 加
    addNumber(e) {
        var id = e.currentTarget.dataset.itemIndex;
        var item = this.data.cartGoods[id]
        var number = item.number + 1
        item.number = number
        this.setData({
            cartGoods: this.data.cartGoods
        })
        this.updateCart(item.product_id, item.goods_id, number, item.id)
    },
    //商品数量改变 接口
    updateCart(productId, goodsId, number, id) {
        util.request(api.CartUpdate, {
            productId: productId,
            goodsId: goodsId,
            number: number,
            id: id
        }, 'POST').then(res => {
            this.setData({
                checkedAllStatus: this.isCheckedAll()
            });
        })
    },
    //删除所选
    deleteCart() {
        var productIds = this.data.cartGoods.filter(item => item.checked)
        if (productIds.length > 0) {
            productIds = productIds.map(item => {
                if (item.checked) {
                    return item.product_id;
                }
            });
            util.request(api.CartDelete, {
                productIds: productIds.join(',')
            }, 'POST').then(res => {
                let cartList = res.data.cartList.map(v => {
                    v.checked = false;
                    return v;
                });
                this.setData({
                    cartGoods: cartList,
                    cartTotal: res.data.cartTotal
                });
                this.setData({
                    checkedAllStatus: this.isCheckedAll()
                });
                if (cartList.length == 0) {
                    this.setData({
                        isEditCart: false
                    })
                    console.log(this.data.isEditCart)
                }
            });
        }
    },
    //下单
    checkoutOrder() {
        var checkedGoods = this.data.cartGoods.filter(item=>item.checked)
        if (checkedGoods.length >0) {
            wx.navigateTo({
                url: '../shopping/checkout/checkout'
            })
        }else{
            wx.showToast({
              title: '请选择要结算的商品',
              icon:'none'
            })
        }
 
    },
    onShow() {
        this.getCartList()
    }

})