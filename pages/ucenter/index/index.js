// pages/ucenter/index/index.js
const util = require('../../../utils/util.js');
const api = require('../../../config/api.js');
const app = getApp()
Page({
    data: {
        userInfo: {},
        showLoginDialog: false,
        menuInfo: [{
                name: "我的订单",
                icon: "order",
                url: "/pages/ucenter/order/order"
            },
            {
                name: "优惠券",
                icon: "coupon",
                url: "/pages/ucenter/coupon/coupon"
            },
            {
                name: "礼品卡",
                icon: "gift",
                url: "url"
            },
            {
                name: "我的收藏",
                icon: "address",
                url: "/pages/ucenter/collect/collect"
            },
            {
                name: "我的足迹",
                icon: "security",
                url: "/pages/ucenter/footprint/footprint"
            },
            {
                name: "会员福利",
                icon: "kefu",
                url: "url"
            },
            {
                name: "地址管理",
                icon: "address",
                url: "../address/address"
            },
            {
                name: "账号安全",
                icon: "security",
                url: "url"
            },
            {
                name: "联系客服",
                icon: "kefu",
                url: "url"
            },
            {
                name: "帮助中心",
                icon: "help",
                url: "url"
            },
            {
                name: "意见反馈",
                icon: "feedback",
                url: "/pages/ucenter/feedback/feedback"
            }
        ]
    },
    //跳转详情页
    gotoItem(e) {
        wx.navigateTo({
            url: e.currentTarget.dataset.url,
        })
    },
    //点击登陆
    onUserInfoClick() {
        if (!wx.getStorageSync('token')) {
            this.setData({
                showLoginDialog: true
            })
        }
    },
    onCloseLoginDialog() {
        this.setData({
            showLoginDialog: false
        })
    },
    onDialogBody() {
        //阻止冒泡
    },
    //微信登陆
    onWechatLogin(e) {
        console.log(e)
        if(e.detail.errMsg==="getUserInfo:ok"){
            wx.login({
                success: (res) => {
                    util.request(api.AuthLoginByWeixin, {
                        code: res.code,
                        userInfo: e.detail
                    }, 'POST').then((_res) => {
                        console.log(_res)
                        this.setData({
                            userInfo: _res.data.userInfo,
                            showLoginDialog: false
                        })
                        wx.setStorageSync('userInfo', JSON.stringify(_res.data.userInfo));
                        wx.setStorageSync('token', _res.data.token);
                    }).catch((err)=>{
                        return err
                    })
                },
                fail: (res => {
                    console.log(res)
                })
            })
        }else{
            wx.showToast({
              title: '您已取消登陆',
            })
        }
       
    },
    onShow(){
        this.setData({
            userInfo:app.globalData.userInfo
        })
    }
})