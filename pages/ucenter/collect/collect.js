// pages/ucenter/collect/collect.js
var util = require('../../../utils/util.js');
var api = require('../../../config/api.js');

var app = getApp();
Page({
    data: {
        typeId: 0,
        collectList: []
    },

    getCollectList() {
        util.request(api.CollectList, {
            typeId: this.data.typeId
        }).then(res => {
            this.setData({
                collectList: res.data.data
            });
        });
    },
    onLoad() {
        this.getCollectList()
    },
    openGoods(e) {
        var touchTime = e.timeStamp - this.data.touch_start
        if (touchTime > 350) {
            wx.showModal({
                title: '',
                content: '确定删除吗？',
                success:(boo)=>{
                    if (boo.confirm) {
                        util.request(api.CollectAddOrDelete, {
                            typeId: this.data.typeId,
                            valueId: e.currentTarget.dataset.value_id
                        }, 'POST').then(res => {
                            wx.showToast({
                                title: '删除成功',
                                icon: 'success',
                                duration: 2000
                            });
                            this.getCollectList();
                        });
                    }
                }
            })
        }
    },
    //按下事件开始  
    touchStart(e) {
        this.setData({
            touch_start: e.timeStamp
        })
    },


})