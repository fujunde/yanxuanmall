// pages/topic/topic.js
var util = require('../../utils/util.js');
var api = require('../../config/api.js');
var app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {
        topicList: [],
        page: 1,
        size: 3,
        count: 0,
        scrollTop: 0,
        showPage: false
    },
    getTopic() {
        this.setData({
            scrollTop: 0,
            showPage: false,
            topicList: []
        });
        // 页面渲染完成
        wx.showToast({
            title: '加载中...',
            icon: 'loading',
            duration: 2000
        });
        util.request(api.TopicList, {
            page: this.data.page,
            size: this.data.size
        }).then((res) => {
            this.setData({
                scrollTop: 0,
                topicList: res.data.data,        //(1)直接获取数据 进行赋值
               /*  ['topicList[' + this.data.topicList.length + ']']: res.data.data,  //(2)考虑setdata数据过多 */
                showPage: true,
                count: res.data.count
            });
            wx.hideToast(); //隐藏消息提示框
        });
    },
    //上一页
    prevPage() {
        if (this.data.page > 1) {
            this.setData({
                page: parseInt(this.data.page) - 1
            });
            this.getTopic();
        }
    },
    //下一页
    nextPage() {
        if (this.data.page + 1 <= this.data.count / this.data.size) {
            this.setData({
                page: parseInt(this.data.page) + 1
            });
            this.getTopic();  //(1)直接获取数据 进行赋值
           /*  this.setll()            //(2)考虑setdata数据过多 */
        }
    },
    /*  //(2)考虑setdata数据过多 将数组拉成二维数组
    setll() {
        util.request(api.TopicList, {
            page: this.data.page,
            size: this.data.size
        }).then((res) => {
            this.setData({
                ['topicList[' + this.data.topicList.length + ']']: res.data.data,
            })
            console.log(this.data.topicList)
        });
    }, */
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getTopic();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})