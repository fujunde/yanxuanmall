//封装ajax
function request(url, data = {}, method = 'GET') {
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/json',
        'X-Nideshop-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        if (res.statusCode >= 200 && res.statusCode < 300 || res.statusCode == 304) {
          if (res.data.errno === 0) {
            resolve(res.data)
          } else {
            wx.showToast({
              title: res.data.errmsg,
              image: '/static/images/icon_error.png',
              mask: true
            })
            reject(res)
          }
        } else {
          wx.showToast({
            title: res.errMsg,
            image: '/static/images/icon_error.png',
            mask: true
          })
          reject(res)
        }
      },
      fail: function (err) {
        wx.showToast({
          title: 'err',
          icon: "none"
        })
        reject(err)
      }
    })
  })
}

module.exports = {
  request: request
}